/*
 * Hash table implementation using glibc's hcreate_r and friends
 *
 * This is a very poor thing that doesn't really do deletes or
 * clean destroys.
 */
#define _GNU_SOURCE
#include <search.h>
#include <stdlib.h>
#include <string.h>

#include "htable.h"

#ifdef HT_OPS_INCLUDE
#define Visibility static
#else
#define Visibility
#endif


struct ht_data_s {
    struct hsearch_data hsd;
};

static void _hti_del(ht_item_t * hti)
{
    free((char *)hti->name);
    free((void *)hti->data);
    free(hti);
}

static ht_item_t *_hti_new(const char *key, int type,
                           const void *val, unsigned int len)
{
    ht_item_t      *hti;

    hti = malloc(sizeof(ht_item_t));
    if (!hti)
        return NULL;

    hti->type = type;
    hti->name = strdup(key);
    hti->data = malloc(len);
    if (!hti->name || !hti->data)
    {
        _hti_del(hti);
        return NULL;
    }

    hti->size = len;
    memcpy(hti->data, val, len);

    return hti;
}

static ht_item_t *_hti_data_replace(ht_item_t * hti, int type,
                                    const void *val, unsigned int len)
{
    hti->type = type;
    hti->data = realloc(hti->data, len);
    hti->size = len;
    memcpy(hti->data, val, len);
    return hti;
}

static ht_item_t *_hti_data_delete(ht_item_t * hti)
{
    free(hti->data);
    hti->data = NULL;
    hti->size = 0;
    return hti;
}

Visibility ht_item_t *ht1_find(ht_data_t * ht, const char *key)
{
    ht_item_t      *hti;
    int             ok;
    ENTRY           itm, *res;

    itm.key = (char *)key;
    itm.data = NULL;
    ok = hsearch_r(itm, FIND, &res, &ht->hsd);
    if (!ok || !res->data)
        return NULL;

    hti = res->data;
    return hti->data ? hti : NULL;
}

Visibility int ht1_add(ht_data_t * ht, const char *key, int type,
                       const void *val, unsigned int len)
{
    int             ok;
    ENTRY           itm, *res;
    ht_item_t      *hti;

    itm.key = (char *)key;
    itm.data = NULL;
    ok = hsearch_r(itm, FIND, &res, &ht->hsd);
    if (ok && res->data)
        hti = _hti_data_replace(res->data, type, val, len);
    else
        hti = _hti_new(key, type, val, len);
    itm.key = (char *)hti->name;
    itm.data = hti;
    ok = hsearch_r(itm, ENTER, &res, &ht->hsd);

    return !ok;
}

Visibility int ht1_del(ht_data_t * ht, const char *key)
{
    int             ok;
    ENTRY           itm, *res;
    ht_item_t      *hti;

    itm.key = (char *)key;
    itm.data = NULL;
    ok = hsearch_r(itm, FIND, &res, &ht->hsd);
    if (!ok || !res->data)
        return 0;

    hti = _hti_data_delete(res->data);

    itm.key = (char *)hti->name;
    itm.data = hti;
    ok = hsearch_r(itm, ENTER, &res, &ht->hsd);

    return !ok;
}


Visibility ht_data_t *ht1_create(void)
{
    ht_data_t      *ht;
    int             ok;

    ht = calloc(1, sizeof(ht_data_t));
    if (!ht)
        return NULL;

    ok = hcreate_r(100, &ht->hsd);
    if (!ok)
    {
        free(ht);
        ht = NULL;
    }

    return ht;
}

Visibility void ht1_destroy(ht_data_t * ht)
{
    hdestroy_r(&ht->hsd);
    free(ht);
}

#ifdef HT_OPS_INCLUDE
#include "htable-switch.h"

extern ht_ops_t ht_ops_glibc;

ht_ops_t        ht_ops_glibc = {
    ht1_create,
    ht1_destroy,

    ht1_find,
    ht1_add,
    ht1_del,
    NULL,
};

#endif /* HT_OPS_INCLUDE */
