/*
 * Hash table imlementation switcher for testing
 */
#ifndef HTABLE_SWITCH_H
#define HTABLE_SWITCH_H


typedef struct {
    ht_data_t      *(*ht_create) (void);
    void            (*ht_destroy) (ht_data_t * ht);

    ht_item_t      *(*ht_find) (ht_data_t * ht, const char *key);
    int             (*ht_add) (ht_data_t * ht, const char *key, int type,
                               const void *val, unsigned int len);
    int             (*ht_del) (ht_data_t * ht, const char *key);
    void            (*ht_iter) (ht_data_t * ht, ht_iter_f * func, void *ctx);
} ht_ops_t;

void            htsw_impl_select(int which);

ht_data_t      *htsw_create(void);
void            htsw_destroy(ht_data_t * ht);

ht_item_t      *htsw_find(ht_data_t * ht, const char *key);

int             htsw_add(ht_data_t * ht, const char *key, int type,
                         const void *val, unsigned int len);
int             htsw_del(ht_data_t * ht, const char *key);

void            htsw_iter(ht_data_t * ht, ht_iter_f * func, void *ctx);

#endif /* HTABLE_SWITCH_H */
