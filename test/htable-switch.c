/*
 * Hashtable implementation switcher
 */
#include "htable.h"
#include "htable-switch.h"

extern ht_ops_t ht_ops_glibc;
extern ht_ops_t ht_ops_uthash;

ht_ops_t       *ops = &ht_ops_uthash;

void htsw_impl_select(int which)
{
    switch (which)
    {
    default:
        ops = &ht_ops_uthash;
        break;
    case 'g':
        ops = &ht_ops_glibc;
        break;
    }
}

ht_data_t      *htsw_create(void)
{
    return ops->ht_create();
}

void htsw_destroy(ht_data_t * ht)
{
    ops->ht_destroy(ht);
}

ht_item_t      *htsw_find(ht_data_t * ht, const char *key)
{
    return ops->ht_find(ht, key);
}

int htsw_add(ht_data_t * ht, const char *key, int type,
             const void *val, unsigned int len)
{
    return ops->ht_add(ht, key, type, val, len);
}

int htsw_del(ht_data_t * ht, const char *key)
{
    return ops->ht_del(ht, key);
}


void htsw_iter(ht_data_t * ht, ht_iter_f * func, void *ctx)
{
    if (ops->ht_iter)
        ops->ht_iter(ht, func, ctx);
}
