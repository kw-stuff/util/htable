/*
 * Hash lib test
 */
#include <stdio.h>
#include <string.h>

#include "htable.h"
#include "htable-switch.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

const char     *const keys1[] = {
    "abc",
    "def",
    "ghijkl",
    "mno"
};

static void show_all(void *ctx, ht_item_t * hti)
{
    printf(" key '%s': '%s'\n", hti->name, (char *)hti->data);
}

int main(int argc, char **argv)
{
    const char     *s;
    unsigned int    i;
    int             err;
    ht_data_t      *htd;
    ht_item_t      *hti;
    const char     *key;

    for (;;)
    {
        argc--;
        argv++;
        if (argc <= 0)
            break;
        s = argv[0];
        if (*s++ != '-')
            break;
        switch (*s)
        {
        case 'u':
            printf("Select %c\n", *s);
            htsw_impl_select(*s);
            break;
        }
    }

    htd = htsw_create();
    if (!htd)
        printf("Could not create htd\n");

    for (i = 0; i < ARRAY_SIZE(keys1); i++)
    {
        printf("Add key '%s'\n", keys1[i]);
        err = htsw_add(htd, keys1[i], 0, keys1[i], strlen(keys1[i]) + 1);
        if (err)
            printf(" - Failed\n");
    }

    htsw_iter(htd, show_all, NULL);

    for (i = 0; i < (unsigned int)argc; i++)
    {
        key = argv[i];
        if (*key == '-')
        {
            key++;
            printf("Delete key '%s'\n", key);
            err = htsw_del(htd, key);
            if (err)
                printf(" - Failed\n");
        }
        else
        {
            printf("Find key '%s'\n", key);
            hti = htsw_find(htd, key);
            if (!hti)
                printf(" - Failed\n");
        }
    }

    htsw_iter(htd, show_all, NULL);

    htsw_destroy(htd);

    return 0;
}
