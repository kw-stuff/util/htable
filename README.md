Simple hashtable module
=======================

A simple hashtable implementation based on [uthash](http://troydhanson.github.com/uthash/)

Overview
--------

* homepage:    https://TBD
* source code: https://TBD

Author
------

Kim Woelders <kim@woelders.dk>


License
-------

uthash.h is licensed as stated in the file.

Everything else is licensed according to COPYING.
