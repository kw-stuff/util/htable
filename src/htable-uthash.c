/*
 * Hash table implementation using uthash
 */
#include <stdlib.h>
#include <string.h>

#include "htable.h"
#include "uthash.h"


typedef struct ht_rec_s ht_rec_t;

struct ht_rec_s {
    UT_hash_handle  hh;
    ht_item_t       itm;
};

struct ht_data_s {
    ht_rec_t       *list;
};

static void _htr_del(ht_rec_t * htr)
{
    free((char *)htr->itm.name);
    free((void *)htr->itm.data);
    free(htr);
}

static ht_rec_t *_htr_new(const char *key, int type,
                          const void *val, unsigned int len)
{
    ht_rec_t       *htr;

    htr = malloc(sizeof(ht_rec_t));
    if (!htr)
        return NULL;

    htr->itm.type = type;
    htr->itm.name = strdup(key);
    htr->itm.data = malloc(len);
    if (!htr->itm.name || !htr->itm.data)
    {
        _htr_del(htr);
        return NULL;
    }

    htr->itm.size = len;
    memcpy(htr->itm.data, val, len);

    return htr;
}

static ht_rec_t *_htr_data_replace(ht_rec_t * htr, int type,
                                   const void *val, unsigned int len)
{
    htr->itm.type = type;
    htr->itm.data = realloc(htr->itm.data, len);
    htr->itm.size = len;
    memcpy(htr->itm.data, val, len);
    return htr;
}

static ht_rec_t *_htr_find(ht_data_t * ht, const char *key)
{
    ht_rec_t       *htr;

    if (!key)
        return NULL;

    HASH_FIND_STR(ht->list, key, htr);

    return htr;
}


ht_item_t      *ht_find(ht_data_t * ht, const char *key)
{
    ht_rec_t       *htr;

    htr = _htr_find(ht, key);

    return htr ? &htr->itm : NULL;
}

int ht_add(ht_data_t * ht, const char *key, int type,
           const void *val, unsigned int len)
{
    ht_rec_t       *htr;

    htr = _htr_find(ht, key);
    if (htr)
    {
        htr = _htr_data_replace(htr, type, val, len);
    }
    else
    {
        htr = _htr_new(key, type, val, len);
        HASH_ADD_KEYPTR(hh, ht->list,
                        htr->itm.name, strlen(htr->itm.name), htr);
    }

    return htr == NULL;
}

int ht_del(ht_data_t * ht, const char *key)
{
    ht_rec_t       *htr;

    htr = _htr_find(ht, key);
    if (!htr)
        return 0;

    HASH_DEL(ht->list, htr);
    _htr_del(htr);

    return 0;
}

void ht_iter(ht_data_t * ht, ht_iter_f * func, void *ctx)
{
    ht_rec_t       *htr, *tmp;

    HASH_ITER(hh, ht->list, htr, tmp)
    {
        func(ctx, &htr->itm);
    }
}


ht_data_t      *ht_create(void)
{
    ht_data_t      *ht;

    ht = calloc(1, sizeof(ht_data_t));
    if (!ht)
        return NULL;

    return ht;
}

void ht_destroy(ht_data_t * ht)
{
    ht_rec_t       *htr, *tmp;

    HASH_ITER(hh, ht->list, htr, tmp)
    {
        HASH_DEL(ht->list, htr);
        _htr_del(htr);
    }
    free(ht);
}

#ifdef HT_OPS_INCLUDE
#include "htable-switch.h"

extern ht_ops_t ht_ops_uthash;

ht_ops_t        ht_ops_uthash = {
    ht_create,
    ht_destroy,

    ht_find,
    ht_add,
    ht_del,

    ht_iter,
};

#endif /* HT_OPS_INCLUDE */
