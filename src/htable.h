/*
 * Hash table API
 */
#ifndef HTABLE_H
#define HTABLE_H

#ifndef extern_C
#ifdef __cplusplus
#define extern_C extern "C"
#else
#define extern_C
#endif
#endif

typedef struct ht_data_s ht_data_t;

typedef struct {
    unsigned char   type;
    unsigned char   rsvd;
    unsigned short  size;
    char           *name;
    void           *data;
} ht_item_t;

typedef void    (ht_iter_f) (void *ctx, ht_item_t * hti);

extern_C ht_data_t *ht_create(void);
extern_C void   ht_destroy(ht_data_t * ht);

extern_C ht_item_t *ht_find(ht_data_t * ht, const char *key);

extern_C int    ht_add(ht_data_t * ht, const char *key, int type,
                       const void *val, unsigned int len);
extern_C int    ht_del(ht_data_t * ht, const char *key);

extern_C void   ht_iter(ht_data_t * ht, ht_iter_f * func, void *ctx);

#endif /* HTABLE_H */
