#include <gtest/gtest.h>

#include "htable.h"

#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

const char     *const keys1[] = {
    "abc",
    "def",
    "ghijkl",
    "mno"
};

static void key_show(void *ctx, ht_item_t * hti)
{
    printf(" show key '%s': '%s'\n", hti->name, (char *)hti->data);
}


static ht_data_t *htd;

static void key_find(const char *key, int exp_ok)
{
    ht_item_t      *hti;

    hti = ht_find(htd, key);
    if (hti)
        printf(" find key '%s': '%s'\n", key, (char *)hti->data);
    else
        printf(" find key '%s': NO\n", key);
    if (exp_ok)
        EXPECT_TRUE(hti != NULL);
    else
        EXPECT_TRUE(hti == NULL);
}

static void key_add(const char *key)
{
    int             err;

    printf(" add  key '%s'\n", key);
    err = ht_add(htd, key, 0, key, strlen(key) + 1);
    EXPECT_EQ(err, 0);
}

static void key_del(const char *key, int exp_ok)
{
    int             err;

    printf(" del  key '%s'\n", key);
    err = ht_del(htd, key);
    if (exp_ok)
        EXPECT_EQ(err, 0);
    else
        EXPECT_NE(err, 0);
}


TEST(PW, htable_create_destroy)
{
    htd = ht_create();
    EXPECT_TRUE(htd != NULL);

    ht_destroy(htd);
}

TEST(PW, htable_test1_00)
{
    htd = ht_create();
    EXPECT_TRUE(htd != NULL);
}

TEST(PW, htable_test1_10)
{
    int             i, err;

    for (i = 0; i < (int)ARRAY_SIZE(keys1); i++)
    {
        key_add(keys1[i]);
        printf(" add  key '%s'\n", keys1[i]);
        err = ht_add(htd, keys1[i], 0, keys1[i], strlen(keys1[i]) + 1);
        EXPECT_TRUE(err == 0);
    }
}

TEST(PW, htable_test1_20)
{
    ht_iter(htd, key_show, NULL);
}

TEST(PW, htable_test1_40)
{
    key_find("", 0);

    key_find("abc", 1);
    key_find("def", 1);
    key_find("zzz", 0);

    key_add("abc");
    key_add("abc");
    key_find("abc", 1);

    key_del("abc", 1);
    key_find("abc", 0);
}

TEST(PW, htable_test1_98)
{
    ht_iter(htd, key_show, NULL);
}

TEST(PW, htable_test1_99)
{
    ht_destroy(htd);
}

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
